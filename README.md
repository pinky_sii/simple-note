# Hackathon I #
(Rapid App Dev Tri 3/2017 - 2018)
### Due: May 6th, 2018 @ 11:59pm ###
You will continue to work on the simple note app started in class by adding the requested features listed below.

## Submission ##
Each student must submit their own version of the app by submitting the url of a public Git repository (e.g github, bitbucket) on Canvas.

## Requested Features ##
1. Available online -- You should properly deploy your app.
2. Verification email -- When a user signs up, your app should verify the email address by sending a verification email.
3. Forget password -- Add forget password functionality for users.
4. User Profile Page -- Add a page that the user can update their profile that contains AT LEAST the following fields:

* Name
* Email
* Password

5. Delete Note -- Add a delete note button
6. Sign in with Facebook/Google -- Allow users to sign in via Facebook or Google.
7. [Extra Credit] Reorder Note -- Design and implement a way for users to rearrange their notes. How about drag & drop? the notes.
8. [Extra Credit] Better UI -- Create a better UI for the App.
## Visit Simple-note here ##
https://simple-note-c8260.firebaseapp.com

## Resources ##
https://firebase.google.com/docs/auth/web/manage-users 

https://material-ui-next.com/
