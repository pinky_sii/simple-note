import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import { auth } from '../firebase';
import { withStyles, Button, Paper, Typography, TextField } from 'material-ui';

const MyLink = props => <Link to="/login" {...props} />;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },

    paper: {
        marginTop: '10%',
        maxWidth: 300,
        textAlign: 'center',
        // marginBottom:'15%',
        paddingTop: theme.spacing.unit *8,
        paddingBottom: theme.spacing.unit *8,
        color: theme.palette.text.secondary,

    },

    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 255,
    },

    button: {
        margin: '0.5em',
        color: '#ffffff',
        width: 255,
        textTransform: 'none',
    },
});

class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : "",
            password : ""
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        const { email, password } = this.state;
        auth.createUserWithEmailAndPassword(email, password)
            .then(authUser => {
                auth.currentUser.sendEmailVerification()
                console.log(authUser);
            })
            .catch(authError => {
                alert(authError);
            });
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const { email, password } = this.state;
        const classes = this.props.classes;
        return (

            <Paper className={classes.paper}>
                <Typography variant="headline" color="secondary" gutterBottom>
                        Sign Up
                </Typography>
                <form onSubmit={this.onSubmit} autoComplete="off">
                    <TextField
                        id="email"
                        label="Email"
                        className={classes.textField}
                        value={email}
                        onChange={this.handleChange('email')}
                        margin="normal"
                        type="email"
                    />
                    <br />
                    <TextField
                        id="password"
                        label="Password"
                        className={classes.textField}
                        value={password}
                        onChange={this.handleChange('password')}
                        margin="normal"
                        type="password"
                    />
                    <br/>
                    <br/>

                    <Button variant="raised" className={classes.button} color="primary" type="submit">Sign up</Button>
                    <Button variant="raised" className={classes.button} color="secondary" component={MyLink}> BACK</Button>
                </form>
            </Paper>
        );
    }
}

export default withStyles(styles)(Signup);