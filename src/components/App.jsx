import React, { Component } from 'react';
import { Route, withRouter, Link} from 'react-router-dom';
import { auth } from '../firebase';
import {withStyles,MuiThemeProvider,createMuiTheme} from 'material-ui/styles';
import {AppBar, Toolbar, Typography, Button, CircularProgress, Grid, IconButton} from "material-ui";
import  Account from '@material-ui/icons/AccountCircle';
import  Notes from '@material-ui/icons/SpeakerNotes';

import PrivateRoute from './PrivateRoute';
import Main from './Main';
import Login from './Login';
import Signup from './Signup';
import UserProfile from './UserProfile'
import MenuButton from './MenuButton';
import image from '../images/sarah.jpg'

const MyLink = props => <Link to="/profile" {...props} />;

const styles = theme => ({

    root: {
        backgroundImage: `url(${image})`,
        backgroundAttachment: 'fixed',
        backgroundSize: 'cover',
        height: '100%',
    },
    loader:{
        position: 'absolute',
        alignItems: 'center',
        top: '50%',
        left: '50%',
    },
    flex: {
        flex: 1,
        color:'#FFFFFF',
    },

    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});

const theme = createMuiTheme({
    palette: {
        primary: {
            main:'#FDD835',
        },
        secondary: {
            main:'#9E9E9E' ,
        },
    },
});

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            authenticated: false,
            currentUser: null,
        };
    }

    handleClick = () => {
        this.props.history.push('/')
    };

    componentWillMount() { auth.onAuthStateChanged(user => {
        if (user) {
            this.setState({
                    authenticated: true,
                    currentUser: user,
                    loading: false,
                    },
                () => { this.props.history.push('/') }
            );
        } else {
            this.setState({
                authenticated: false,
                currentUser: null,
                loading: false,
            });
        }

    });
    }

    render () {
        const { classes } = this.props;
        const { authenticated, loading } = this.state;
        const content = loading ? (
            <div  align="center" className={classes.loader}>
                <CircularProgress size={80} thickness={5} />
            </div>
        ) : (
            <div >
                <PrivateRoute
                    exact
                    path="/"
                    component={Main}
                    authenticated={authenticated}
                />
                <PrivateRoute
                    exact
                    path="/profile"
                    component={UserProfile}
                    authenticated={authenticated}
                />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={Signup} />
            </div>
        );

        return (
            <MuiThemeProvider theme={theme}>
                <div className={classes.root}>
                    <AppBar position="fixed" color="primary" >
                        <Toolbar >
                            <Typography variant="headline" className={classes.flex}>
                                Simple Note
                            </Typography>

                            { authenticated &&
                            <div >
                                <IconButton onClick={this.handleClick}> <Notes/> </IconButton>
                                <IconButton component={MyLink} ><Account/></IconButton>
                                <MenuButton />
                            </div>
                            }

                        </Toolbar>
                    </AppBar>

                    <Grid container spacing={0} align="center" >

                            <Grid item xs={12}>
                                <div className={classes.content}>
                                    { content }
                                </div >
                            </Grid>
                    </Grid>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default withRouter(withStyles(styles)(App));