import React from 'react';
import { auth} from '../firebase';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({

    flatbtn: {
        margin: '0.3em',
        color : '#FFC400',
        textTransform: 'none',
    },

});


class ForgotPassDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            email:"",
        };
    }

    handleClickOpen = () => {
        this.setState({ open: true });
        console.log("click");
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
        console.log(event.target.value)
    };

    handleSendPassword = (e) => {
       e.preventDefault();
        const {email} = this.state;
        auth.sendPasswordResetEmail(email).then(function() {
            console.log("email sent to " + email);
        }).catch(function(error) {
            console.error(error)
        });
        this.handleClose()
    };

    render() {
        const {classes} = this.props;
        const {email} = this.state;
        return (
                <div>

                    <Button onClick={this.handleClickOpen} className={classes.flatbtn} >
                        Forgot password?
                    </Button>

                    <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="form-dialog-title"
                    >
                        <DialogTitle id="form-dialog-title">Forgot password?</DialogTitle>

                        <DialogContent>

                            <DialogContentText>
                                 We just need your registered Email to sent you password reset instructions.
                            </DialogContentText>

                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Email Address"
                                type="email"
                                fullWidth
                                val = {email}
                                onChange = {this.handleChange('email')}
                            />

                        </DialogContent>

                        <DialogActions>

                            <Button onClick={this.handleClose} >
                                Cancel
                            </Button>

                            <Button onClick={this.handleSendPassword} className={classes.flatbtn}>
                                RESET PASSWORD
                            </Button>

                        </DialogActions>

                    </Dialog>
                </div>
        );
    }
}

export default withStyles(styles)(ForgotPassDialog);


