import React,{ Component } from 'react';
import {Paper, withStyles, Typography} from 'material-ui'
import { auth } from '../firebase';
import EditProfileDialog from './EditProfileDialog';
import ChangePassword from './ChangePasswordDialog'

const styles = theme => ({

    paper: {
        marginTop: '15%',
        textAlign: 'left',
        maxWidth: 800,
        margin:theme.spacing.unit * 2,
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary,
    },
    changepassbtn:{
        color: '#FFC400',
    }

});


class UserProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            email:"",
        };
    }

    render() {
        const {classes} = this.props;
        return (
            <Paper className={classes.paper}>

                <Typography variant="headline" gutterBottom> My Profile </Typography>
                <Typography gutterBottom> Name : {auth.currentUser.displayName} </Typography>
                <Typography gutterBottom> Email : { auth.currentUser.email } </Typography>
                <Typography gutterBottom> Email verification : { auth.currentUser.emailVerified ? 'Verified' : 'Not verified'} </Typography>
                <div>
                    <Typography gutterBottom> Password : </Typography>
                    <ChangePassword className={classes.changepassbtn}/>
                </div>

                <EditProfileDialog/>

            </Paper>

        );
    }
}

export default withStyles(styles)(UserProfile);