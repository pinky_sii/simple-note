import React from 'react';
import {IconButton}from 'material-ui';
import Menu, { MenuItem } from 'material-ui/Menu';
import Fade from 'material-ui/transitions/Fade';
import  Arrow from '@material-ui/icons/ArrowDropDown';
import {auth} from "../firebase";

class MenuButton extends React.Component {
    state = {
        anchorEl: null,
    };

    handleClick = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const { anchorEl } = this.state;

        return (
            <div style={{display: 'inline'}}>

                <IconButton
                    aria-owns={anchorEl ? 'fade-menu' : null}
                    aria-haspopup="true"
                    onClick={this.handleClick}
                ><Arrow/></IconButton>

                <Menu
                    id="fade-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                    transition={Fade}
                >
                    <MenuItem onClick={() => auth.signOut()}>Logout</MenuItem>
                </Menu>
            </div>
        );
    }
}

export default MenuButton;