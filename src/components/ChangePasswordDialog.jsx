import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import {withStyles} from 'material-ui';
import {auth} from "../firebase";


const styles = theme => ({

    changepassbtn:{
        color: '#FFC400',
        textTransform: 'none',
    },
    cancelbtn:{
        color:'#9E9E9E',
    }

});


class ChangePasswordDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            password:"",
        };
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
        console.log(event.target.value)
    };

    handleUpdate = (e) => {
        e.preventDefault();
        const {password} = this.state;
        if (password !== '') {
           auth.currentUser.updatePassword(password).then(function() {
               alert("Update successful!");
           }).catch(function(error) {
               alert("Error" + error)
           });
        }
        this.handleClose()
    };



    render() {
        const {password} = this.state;
        const {classes} = this.props;
        return (
            <div>

                <Button  onClick={this.handleClickOpen} className={classes.changepassbtn}> change password </Button>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Change Password</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Choose a strong password and don't reuse it for other accounts.
                            Use at least 6 characters.
                        </DialogContentText>

                        <TextField
                            id="password"
                            label="Password"
                            value={password}
                            onChange={this.handleChange('password')}
                            margin="normal"
                            type="password"
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleUpdate} color="primary">
                            Update
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(ChangePasswordDialog)