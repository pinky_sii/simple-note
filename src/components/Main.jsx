import React, { Component } from 'react';
import { auth, db } from '../firebase';
import { withStyles } from 'material-ui/styles';
import {Delete,Add} from '@material-ui/icons';
import { Button,Paper,TextField,IconButton,Typography, Tooltip }from 'material-ui';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import List, {
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
} from 'material-ui/List';


// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

const grid = 1;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid ,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? '#FDD835' : 'lightgrey',

    // styles we need to apply on draggables
    ...draggableStyle,
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'white' : 'white',
    padding: grid,
    width: 300,
});


const styles = theme => ({

    paper: {
        marginTop: '15%',
        margin:theme.spacing.unit * 2,
        maxWidth: 800,
        padding: theme.spacing.unit * 3,
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    textField: {
        width: 240,
    },
    list: {
        maxWidth: 760,
        maxHeight: 200,
        overflow: 'auto',
    },
    button: {
        margin: theme.spacing.unit,
        color: '#9E9E9E'
    },

});

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notes : [],
            current : ""
        };
        this.addNote = this.addNote.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    componentWillMount() {
        const uid = auth.currentUser.uid;
        let notesRef = db.ref('notes/' + uid).orderByKey().limitToLast(100);
        notesRef.on('child_added', snapshot => {
            let note = { text: snapshot.val(), id: snapshot.key };
            this.setState({ notes: [note].concat(this.state.notes) });
        });

        notesRef.on('child_removed', snapshot => {
            const updateNotes = this.state.notes.filter(note => note.id!==snapshot.key);
            this.setState({notes : updateNotes});
        });

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    addNote(e) {
        e.preventDefault();
        const uid = auth.currentUser.uid;
        db.ref('notes/' + uid).push(this.state.current);
        this.setState({ current : "" });
        console.log("add " + uid)
    }

    deleteNote(key) {
        const uid = auth.currentUser.uid;
        db.ref('notes/' + uid).child(key).remove()
        console.log("remove " + key)
    }

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        const items = reorder(
            this.state.notes,
            result.source.index,
            result.destination.index
        );


        this.setState({
            notes : []
        });

        const uid = auth.currentUser.uid;
        db.ref('notes/' + uid).remove();

        for (var item = items.length-1;item>=0; item --) {
            db.ref('notes/' + uid).push(items[item].text)
        }

    }

    render() {
        const classes = this.props.classes;
        return (

                    <Paper className={classes.paper}>
                        <Typography variant="title"> Hello, {auth.currentUser.displayName} </Typography>
                        <form onSubmit={this.addNote}>
                            <TextField
                                id="note"
                                label="Enter new note"
                                className={classes.textField}
                                value={this.state.current}
                                onChange={this.handleChange('current')}
                                margin="normal"
                            />
                            <Tooltip id="tooltip-fab" title="Add" >
                            <Button variant="fab" mini color="primary" aria-label="add" type="submit" className={classes.button}>
                                <Add/>
                            </Button>
                            </Tooltip>

                        </form>



                        <DragDropContext onDragEnd={this.onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={getListStyle(snapshot.isDraggingOver)}
                                    >
                                        <List className={classes.list}>
                                        {this.state.notes.map((item, index) => (
                                            <Draggable key={item.id} draggableId={item.id} index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}
                                                    >
                                                        <ListItem key={item.id}>
                                                            <ListItemText primary={(index+1) + '. ' + item.text}/>
                                                            <ListItemSecondaryAction>
                                                                <IconButton aria-label="Delete"  onClick={()=>{this.deleteNote(item.id)}}>
                                                                    <Delete/>
                                                                </IconButton>
                                                            </ListItemSecondaryAction>
                                                        </ListItem>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        </List>
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>

                    </Paper>

        );
    }
}

export default withStyles(styles)(Main);
