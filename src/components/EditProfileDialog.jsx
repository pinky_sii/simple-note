import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import {auth} from "../firebase";

export default class EditProfileDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            name:'',
            email:"",
        };
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
        console.log(event.target.value)
    };

    handleUpdate = (e) => {
        e.preventDefault();
        const {email,name} = this.state;
        if (email !== '') {
            auth.currentUser.updateEmail(email).then(authUser => {
                auth.currentUser.sendEmailVerification()
                console.log(authUser);
            }).catch(authError => {
                alert(authError);
            });
        }

        if (name !=='') {
            auth.currentUser.updateProfile({displayName: name})
        }

        this.handleClose()
        
    };



    render() {
        const {email,name} = this.state;
        return (
            <div>

                <Button  onClick={this.handleClickOpen}> Edit Profile </Button>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Edit Profile</DialogTitle>
                    <DialogContent>

                        <TextField
                            id="name"
                            label="Name"
                            value={this.state.name}
                            onChange={this.handleChange('name')}
                            fullWidth
                            margin="normal"
                        />

                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Email Address"
                            type="email"
                            fullWidth
                            onChange={this.handleChange('email')}
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleUpdate} color="primary">
                            Update
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}