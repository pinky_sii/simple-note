import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { auth, fbProvider, googleProvider } from '../firebase';
import ForgotPassDialog from './ForgotPassDialog'

import { withStyles, createMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import {Button, Paper, TextField, Typography} from 'material-ui';

const styles = theme => ({

    paper: {
        marginTop: '7%',
        maxWidth: 300,
        paddingTop: theme.spacing.unit *6,
        paddingBottom: theme.spacing.unit *6,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },

    button: {
        margin: '0.3em',
        color: '#ffffff',
        width: 255,
        textTransform: 'none',
    },

    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 255,
    },

    divider: {
        width: 120,
    },

});

const theme = createMuiTheme({
    palette: {
        primary: {main: '#BE2612'},
        secondary: {main: '#4267B2'},
    },
});

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : "",
            password : "",
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        const { email, password } = this.state;
        auth.signInWithEmailAndPassword(email, password)
            .then(authUser => {
                console.log(authUser);
            })
            .catch(authError => {
                alert(authError);
            })
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    authWithFB(event) {
        event.preventDefault();
        auth.signInWithPopup(fbProvider)
            .then(authUser => {
                console.log(authUser);
            })
            .catch(authError => {
                alert(authError);
            })
    }

    authWithGoogle(event) {
        event.preventDefault();
        auth.signInWithPopup(googleProvider)
            .then(authUser => {
                console.log(authUser + "yasssss");
            })
            .catch(authError => {
                alert(authError);
            })
    }


    render() {
        const { email, password } = this.state;
        const classes = this.props.classes;
        return (
            < MuiThemeProvider theme={theme}>

                <Paper className={classes.paper}>

                    <div>
                        <Button  className={classes.button} variant="raised" color="secondary" onClick={this.authWithFB}> Continue with Facebook </Button>
                        <br/>
                        <Button  className={classes.button} variant="raised" color="primary" onClick={this.authWithGoogle}> Continue with Google </Button>

                    </div>
                    <br/>

                    <Typography> or </Typography>

                    <form onSubmit={this.onSubmit} autoComplete="off">

                        <TextField
                            id="email"
                            label="Email"
                            className={classes.textField}
                            value={email}
                            onChange={this.handleChange('email')}
                            margin="normal"
                            type="email"
                        />
                        <br />
                        <TextField
                            id="password"
                            label="Password"
                            className={classes.textField}
                            value={password}
                            onChange={this.handleChange('password')}
                            margin="normal"
                            type="password"
                        />
                        <br/>
                        <ForgotPassDialog/>
                        <br/>
                        <Button variant="raised" className={classes.button} type="submit">Log in</Button>
                    </form>

                        <br/>

                            <Typography variant="body1" gutterBottom align="center">
                                Don't have an account? <Link to="/signup">Sign up here</Link>
                            </Typography>

                </Paper>

            </MuiThemeProvider>
        );
    }
}

export default withStyles(styles)(Login);